<?php

namespace PhpIntegrator\Refactoring;

/**
 * Indicates an appropriate use statement already exists.
 */
class UseStatementAlreadyExistsException extends UseStatementInsertionCreationException
{

}
