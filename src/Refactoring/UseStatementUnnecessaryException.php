<?php

namespace PhpIntegrator\Refactoring;

/**
 * Indicates the use statement is unnecessary in the active namespace.
 */
class UseStatementUnnecessaryException extends UseStatementInsertionCreationException
{

}
