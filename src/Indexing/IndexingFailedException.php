<?php

namespace PhpIntegrator\Indexing;

use RuntimeException;

/**
 * Exception that indicates that indexing failed.
 */
final class IndexingFailedException extends RuntimeException
{

}
