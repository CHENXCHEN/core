<?php

namespace PhpIntegrator\Indexing;

/**
 * Storage exception that indicates a file wasn't found.
 */
final class FileNotFoundStorageException extends StorageException
{

}
