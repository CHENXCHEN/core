<?php

namespace A;

/**
 * @return array
 */
function some_function_correct(): array
{

}

/**
 * @return string
 */
function some_function_parameter_incorrect_type(): ?string
{

}
